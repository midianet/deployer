package midianet.deployer.resource;

import midianet.deployer.model.Produto;
import midianet.deployer.repository.ProdutoRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/produtos")
public class ProdutoResource {

    @Autowired
    private ProdutoRepository repository;

    @PostMapping
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public void post(@RequestBody Produto produto , HttpServletResponse response) {
        produto.setId(null);
        produto = repository.save(produto);
        response.setHeader(HttpHeaders.LOCATION, ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(produto.getId()).toUri().toString());
    }

    @Transactional
    @PutMapping(path = "/{id}")
    public void put(@PathVariable Long id, @RequestBody Produto produto) {
        var persistent = get(id);
        BeanUtils.copyProperties(produto, persistent, "id");
        repository.save(persistent);
    }

    @Transactional
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        repository.delete(get(id));
    }

    @GetMapping("/{id}")
    public Produto get(@PathVariable Long id) {
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format("Produto %s não encontrado", id )));
    }

    @GetMapping
    public List<Produto> get() {
        return repository.findAll();
    }

}